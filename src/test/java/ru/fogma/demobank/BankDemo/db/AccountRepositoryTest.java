package ru.fogma.demobank.BankDemo.db;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import ru.fogma.demobank.BankDemo.model.TransactionDTO;
import ru.fogma.demobank.BankDemo.service.TransactionService;


import java.math.BigDecimal;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(initializers = {AccountRepositoryTest.Initializer.class})
public class AccountRepositoryTest {

    private final UUID sourceUUID = UUID.fromString("43e8a3e9-56ad-4217-87a1-17e6999ddfed");
    //    private final UUID sourceUUID = UUID.fromString("43923e92-5d18-49de-91a8-0fb28bfa0d08");
    private final UUID targetUUID = UUID.fromString("eafcdcd1-8d74-4096-8d8a-fca03d6aebe6");
//    private final UUID targetUUID = UUID.fromString("b9661cce-b839-4d78-be7a-fc865480fedc");

    TransactionRepository transactionRepository = mock(TransactionRepository.class);
    AccountRepository accountRepository = mock(AccountRepository.class);
    TransactionService transactionService = new TransactionService(accountRepository, transactionRepository);


    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer("postgres")
            .withDatabaseName("banking")
            .withUsername("postgres")
            .withPassword("pgadmin");

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }



    public void should_throw_optimistic_lock_exception() {
        insertAccounts();
        for (int i = 0; i < 10; i++) {
            new Thread(() -> accountRepository.save(new Account(sourceUUID, "Vasyan", new BigDecimal("11111"), 1))).start();
//            new Thread(() -> accountRepository.updateAccountBalanceByUUID(sourceUUID, new BigDecimal("11111"))).start();
//            Runnable worker = () -> transactionService.transfer(getTransactionDTO());
//            Runnable worker = () -> accountRepository.updateAccountBalanceByUUID(sourceUUID, new BigDecimal("11111"));
//            executor.execute(worker);
        }
//        executor.shutdown();
//        while (!executor.isTerminated()) {}
    }


    private Account getAccountOne() {
        return new Account(sourceUUID, "Petruha Vasechkin", new BigDecimal("88888"), 1);
    }

    private Account getAccountTwo() {
        return new Account(targetUUID, "Vaska Petrushkin", new BigDecimal("22222"), 1);
    }

    private TransactionDTO getTransactionDTO() {
        return new TransactionDTO(sourceUUID, targetUUID, new BigDecimal("1"));
    }

    private void insertAccounts() {
        accountRepository.save(getAccountOne());
        accountRepository.save(getAccountTwo());
        accountRepository.flush();
    }

}